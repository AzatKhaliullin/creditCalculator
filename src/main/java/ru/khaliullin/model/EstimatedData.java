package ru.khaliullin.model;

import org.apache.commons.math3.util.Precision;

public class EstimatedData {
    private int paymentNumber;
    private String monthsYears;
    private double mainDebt;
    private double interestPayment;
    private double mainBalanceDebt;
    private double totalPaymentAmount;

    public EstimatedData() {
    }

    public EstimatedData(int paymentNumber, String monthsYears,
                         double mainDebt, double interestPayment,
                         double mainBalanceDebt, double totalPaymentAmount) {
        this.paymentNumber = paymentNumber; // Номер платежа
        this.monthsYears = monthsYears; // Месяц/Год
        this.mainDebt = mainDebt; // Платеж по основному долгу
        this.interestPayment = interestPayment; // Платеж по процентам
        this.mainBalanceDebt = mainBalanceDebt; // Остаток основного долга
        this.totalPaymentAmount = totalPaymentAmount; // Общая сумма платежа
    }

    public int getPaymentNumber() {
        return paymentNumber;
    }

    public void setPaymentNumber(int paymentNumber) {
        this.paymentNumber = paymentNumber;
    }

    public String getMonthsYears() {
        return monthsYears;
    }

    public void setMonthsYears(String monthsYears) {
        this.monthsYears = monthsYears;
    }

    public double getMainDebt() {
        return mainDebt;
    }

    public void setMainDebt(double mainDebt) {
        this.mainDebt = Precision.round(mainDebt,2);
    }

    public double getInterestPayment() {
        return interestPayment;
    }

    public void setInterestPayment(double interestPayment) {
        this.interestPayment = Precision.round(interestPayment,2);
    }

    public double getMainBalanceDebt() {
        return mainBalanceDebt;
    }

    public void setMainBalanceDebt(double mainBalanceDebt) {
        this.mainBalanceDebt = Precision.round(mainBalanceDebt,2);
    }

    public double getTotalPaymentAmount() {
        return totalPaymentAmount;
    }

    public void setTotalPaymentAmount(double totalPaymentAmount) {
        this.totalPaymentAmount = Precision.round(totalPaymentAmount,2);
    }
}

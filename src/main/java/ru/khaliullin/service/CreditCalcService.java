package ru.khaliullin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import ru.khaliullin.model.EstimatedData;
import ru.khaliullin.model.LoanData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class CreditCalcService {

    @Autowired
    private Environment environment;

    private double amountOfCredit;
    private double interestRate;
    private int month;
    private int year;

    /**
     * Данный метод производит рассчет аннуитетного графика
     * @param loanData
     * @return
     */
    public List<EstimatedData> calculator(LoanData loanData) {
        List<EstimatedData> newList = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        month = calendar.get(Calendar.MONTH) + 1;
        year = calendar.get(Calendar.YEAR);

        amountOfCredit = loanData.getAmountOfCredit();
        int creditTerm = loanData.getCreditTerm();

        interestRate = Double.parseDouble(environment.getProperty("interest.rate"));

       for(int i = 1; i <= creditTerm; i++){
            EstimatedData newEstimatedData = new EstimatedData();
            // Номер платежа
            newEstimatedData.setPaymentNumber(i);
            // Месяц/Год
            newEstimatedData.setMonthsYears(setMonthYear(month, year));
           // Платеж по процентам
           newEstimatedData.setInterestPayment(interestCharges(newEstimatedData.getTotalPaymentAmount()));
            // Общая сумма платежа
            newEstimatedData.setTotalPaymentAmount(monthlyPayment(amountOfCredit, creditTerm));
            // Платеж по основному долгу
            newEstimatedData.setMainDebt(newEstimatedData.getTotalPaymentAmount()
                                        - newEstimatedData.getInterestPayment());
            // Остаток основного долга
            newEstimatedData.setMainBalanceDebt(amountOfCredit);
            amountOfCredit = amountOfCredit - newEstimatedData.getTotalPaymentAmount();
           newList.add(newEstimatedData);
        }

        return newList;
    }

    /**
     * Данный метод производит установку дат
     *
     * @param monthN
     * @param yearN
     * @return
     */
    private String setMonthYear(int monthN, int yearN) {
        int m = monthN;
        int y = yearN;
        if(m < 12){
            month = month + 1;
            year = year;
        }else{
            month = 1;
            year = year + 1;
        }
        return "" + m + "/" + y;
    }

    /**
     * Данный метод производит расчет месячного платежа
     *
     * @param amountOfCredit
     * @param creditTerm
     * @return
     */
    private double monthlyPayment(double amountOfCredit, int creditTerm) {
        return amountOfCredit*((interestRate/12)/(1 - (Math.pow((1 + (interestRate/12)), -creditTerm))));
    }

    /**
     * Данный метод производит расчет начисленного процента
     * @param totalPaymentAmount
     * @return
     */
    private double interestCharges(double totalPaymentAmount) {
        return (amountOfCredit-totalPaymentAmount)*interestRate/12;
    }
}

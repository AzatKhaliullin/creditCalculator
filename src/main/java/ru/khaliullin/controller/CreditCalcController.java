package ru.khaliullin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.khaliullin.model.EstimatedData;
import ru.khaliullin.model.LoanData;
import ru.khaliullin.service.CreditCalcService;

import java.util.List;

@Controller
public class CreditCalcController {

    @Autowired
    private CreditCalcService creditCalcService;

    @GetMapping("/calc")
    public String calc() {
        return "calc";
    }

    @PostMapping("/calc")
    public ResponseEntity<List<EstimatedData>> calc(@RequestBody LoanData loanData) {

        try {
            List<EstimatedData>  arrayEstimatedData = creditCalcService.calculator(loanData);

            return ResponseEntity.ok().body(arrayEstimatedData);

        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }
}
